#pragma once

#include "global.h"
#include "shader.h"
#include "geometry.h"
#include "scene_graph.h"
#include "light.h"
#include "camera.h"

class RenderObject {
	GLuint vertexBuffer;
	GLuint elementBuffer;
	GLuint vertexArray;
	unsigned int numIndices;

	GLuint diffuseTexture = 0;
	GLuint specularTexture = 0;

	ShaderProgram shader;

	size_t frameOffset;
	int animStartFrameInd = 0;

public:

	unsigned numFrames = 0;

	GLuint renderMode = GL_FILL;

	std::vector<SceneNode*> nodes;

	std::vector<LightSourceData>& lightSources;

	MaterialParams matParams;

	bool lighted = true;

	bool transparent = false;

	float boundSphereRadius = 1.f;

	unsigned int id;

	bool collisionDisabled = false;

	glm::vec2 uvOffset = glm::vec2(0.f);

	int numTexFrames = 0;

	bool animationPaused = true;

	static glm::vec4 globalAmbient;

	RenderObject(const GeometryObject<Vertex>& geom, const ShaderProgram& shader);

	RenderObject(const GeometryObject<PGRVertex>& geom, const ShaderProgram& shader);

	~RenderObject();

	/// <summary>
	/// Renders the object in context of passed scene parameters
	/// </summary>
	/// <param name="camera"></param>
	/// <param name="lightsUBO"></param>
	/// Fog parameters:
	/// <param name="fogTex"></param>
	/// <param name="windowExt"> window width and height</param>
	/// Animation parameters:
	/// <param name="frameInd"></param>
	/// <param name="frameTime"></param>
	void render(Camera& camera, GLuint lightsUBO, GLuint fogTex, glm::vec2 windowExt, int frameInd, float frameTime);

	template<class VertexT>
	void fillElementArrays(const GeometryObject<VertexT>& geom);

	void createTextures(const Material& material);

	/// <summary>
	/// Calculated sphere collisions for all nodes of the object and he specified node of tested object
	/// </summary>
	/// <param name="tested">object to test collisions for</param>
	/// <returns>Normalized sum of all directions to collision points or zero vector</returns>
	glm::vec3 getCollisionVec(RenderObject& tested, SceneNode* testedNode) const;
};
