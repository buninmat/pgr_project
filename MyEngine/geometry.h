#pragma once
#include "global.h"
#include "util.h"
#include "texture.h"

#include "models/wizard/frame_0.h"
#include "models/wizard/frame_1.h"
#include "models/wizard/frame_2.h"
#include "models/wizard/frame_3.h"
#include "models/wizard/frame_4.h"
#include "models/wizard/frame_5.h"
#include "models/cube.h"

struct Vertex {
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec3 color = glm::vec3(1.0f, 1.0f, 1.0f);
	glm::vec2 texCoord;

	bool operator == (const Vertex& other) const;
};

struct PGRVertex {
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec2 texCoord;
};

struct MaterialParams{
	glm::vec4 diffuse = glm::vec4(1.f);
	glm::vec4 specular = glm::vec4(0.5f);
	glm::vec4 ambient = glm::vec4(0.f);
	glm::vec4 emissive = glm::vec4(0.f);
	float sharpness = 10;
};

struct Material {
	std::unique_ptr<Texture> diffuseTexture;
	std::unique_ptr<Texture> specularTexture;
	MaterialParams params;
};

template<class TVertex>
struct GeometryObject {
	std::string name;
	std::vector<TVertex> vertices;
	std::vector<GLuint> indices;
	
	Material material;

	unsigned numFrames = 1;

	GeometryObject(){}


	GeometryObject(std::string name):GeometryObject(name, false, true) {}

	GeometryObject(std::string name, bool useDump):GeometryObject(name, useDump, true) {}

	GeometryObject(std::string name, bool useDump, bool indexed) :name(name) {
		if (useDump) {
			loadDump();
		}else {
			loadObj(indexed);
		}
	}

	void saveDump();

	bool loadDump();

	void loadObj(bool indexed);

	static GeometryObject* makeTriangle() {
		auto* triangle = new GeometryObject();
		triangle->vertices = {
			{{-0.5f, -0.5f, 1.0f}, {1.0f, 0.0f, 0.0f}},
			{{0.5f, -0.5f, 1.0f}, {0.0f, 1.0f, 0.0f}},
			{{0.0f, 0.5f, 1.0f}, {0.0f, 0.0f, 1.0f}}
		};
		triangle->indices = { 0, 1, 2 };
		return triangle;
	}

	static GeometryObject* makeCube();

	static GeometryObject* makeUVSphere(float radius, int steps);

	static GeometryObject* makeTorus(float radius, float ringRadius, int uSteps, int vSteps);

	static GeometryObject* makePlane(int xTiles, int yTiles, float tileSide,
		glm::vec2 uvScale = glm::vec2(1.f, 1.f), glm::vec2 uvOffset = glm::vec2(0.f, 0.f));

	static GeometryObject<PGRVertex>* loadWizard();

	static GeometryObject<PGRVertex>* loadCube();
};

