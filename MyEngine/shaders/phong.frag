#version 460 core

layout(location = 0) in vec2 texCoord;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec3 fragPos;

out vec4 resColor;

struct Material{
	vec4 diffuse;
	vec4 specular;
	vec4 ambient;
	vec4 emissive;
	float sharpness;
};

uniform sampler2D diffuseTexture;

uniform sampler2D specularTexture;

uniform sampler2D fogTexture;

uniform bool lighted;

uniform Material material;

uniform vec4 globalAmbient;

uniform vec3 viewer;

uniform vec2 windowExtent;

struct LightSourceData {
	vec4 pos;
	vec4 direction;
	
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float atten_const;
	float atten_lin;
	float atten_quad;

	float spot_cutoff;
	float spot_exp;

	float intensity;

	int type;
};

layout(std140) uniform LightSourcesData{
	LightSourceData data[100];
	int size;
} lightSources;

vec4 addFog(vec4 fragRes){
	vec4 fogC = texture(fogTexture, gl_FragCoord.xy / windowExtent);
	float vdist = length(fragPos - viewer);
	float factor = max(0, (500.0 - vdist) / 500.0);
	return fogC * (1 - factor) + fragRes * factor;
	return fragRes;
}

void main(){
	vec2 uv = texCoord;
	uv.y = 1 -uv.y;
	vec3 normal = normalize(fragNormal);
	Material mat = material;
	if(mat.diffuse.r < 0){
		mat.diffuse = texture(diffuseTexture, uv);
		mat.ambient *= mat.diffuse;
	}
	if(!lighted){
		resColor = addFog(mat.diffuse);
		resColor.a = mat.diffuse.a;
		return;
	}
	if(mat.specular.r < 0){
		mat.specular = texture(specularTexture, uv);
	}
	vec4 lightsColor = vec4(0);
	for(int i = 0; i < lightSources.size; ++i){
		LightSourceData light = lightSources.data[i];
		vec3 lightDir;
		float dist;
		float attenuation;
		if(light.type == 0){ //directional
			lightDir = normalize(light.direction.xyz);
			attenuation = light.intensity;
		}else {
			lightDir = light.pos.xyz - fragPos;
			dist = length(lightDir);
			lightDir /= dist;
			attenuation = light.intensity / (light.atten_const + light.atten_lin * dist + light.atten_quad * dist * dist);
			if(light.type == 2){ //spot
				float DdotL = dot(light.direction.xyz, lightDir);
				if(DdotL >= cos(light.spot_cutoff)){
					attenuation *= pow(DdotL, light.spot_exp);
				}else{
					attenuation = 0.;
				}
			}
		}
		float NdotL = dot(normal, lightDir);
		vec4 diffuse = mat.diffuse * light.diffuse * max(NdotL, 0);
		vec3 refl = reflect(lightDir, normal); 
		float VdotR = dot(normalize(viewer - fragPos), refl);
		vec4 specular = mat.specular * light.specular * pow(max(VdotR, 0), mat.sharpness);
		vec4 ambient = mat.ambient * light.ambient;
		
		lightsColor += (ambient + diffuse + specular) * attenuation;
	}
	
	resColor = addFog(lightsColor + globalAmbient + mat.emissive);
	resColor.a = mat.diffuse.a;
}