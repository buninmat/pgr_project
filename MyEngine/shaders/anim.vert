#version 460 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 3) in vec3 posNext;
layout(location = 4) in vec3 normalNext;

layout(location = 5) in vec3 posNextNext;
layout(location = 6) in vec3 normalNextNext;

layout(location = 0) out vec2 texCoord;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec3 fragPos;

struct Transform{
	mat4 model;
	mat4 view;
	mat4 projection;
};

uniform Transform transform;

uniform float mixT;

uniform bool stopped;

uniform bool useNextNext;

void main(){
	vec4 posInterp;
	vec3 normInterp;
	if(stopped){
		posInterp = vec4(pos, 1.0);
		normInterp = normal;
	}else if(useNextNext){
		posInterp = vec4(mix(pos, posNextNext, mixT), 1.0);
		normInterp = normalize(mix(normal, normalNextNext, mixT));
	}else{
		posInterp = vec4(mix(pos, posNext, mixT), 1.0);
		normInterp = normalize(mix(normal, normalNext, mixT));
	}
	vec4 worldPos = transform.model * posInterp;
	gl_Position = transform.projection * transform.view * worldPos;
	texCoord = inTexCoord;
	fragNormal = normalize(transpose(inverse(mat3(transform.model))) * normInterp);
	fragPos = worldPos.xyz;
}