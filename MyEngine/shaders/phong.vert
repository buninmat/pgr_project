#version 460 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec2 texCoord;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec3 fragPos;

struct Transform{
	mat4 model;
	mat4 view;
	mat4 projection;
};

uniform Transform transform;

uniform vec2 uvOffset;

void main(){
	vec4 worldPos = transform.model * vec4(pos, 1.0f);
	gl_Position = transform.projection * transform.view * worldPos;
	texCoord = inTexCoord + uvOffset;
	fragNormal = normalize(transpose(inverse(mat3(transform.model))) * normal);
	fragPos = worldPos.xyz;
}