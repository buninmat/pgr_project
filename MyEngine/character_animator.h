#pragma once
#include "render_object.h"

class CharacterAnimator {

	RenderObject& characterObject;
	RenderObject& planeObj;
	SceneNode* movedNode;

	SceneNode* worldMovedNode;

	std::list<RenderObject>& collisionObjects;

	glm::vec3 sceneBndLower;
	glm::vec3 sceneBndUpper;


public:

	bool endlessMode = true;

	CharacterAnimator(RenderObject& characterObject, RenderObject& planeObject, SceneNode* movedNode, SceneNode* worldMovedNode, std::list<RenderObject>& collisionObjects,
		glm::vec3 sceneBndLower, glm::vec3 sceneBndUpper)
		: characterObject(characterObject), planeObj(planeObject), movedNode(movedNode), worldMovedNode(worldMovedNode),
		collisionObjects(collisionObjects),
		sceneBndLower(sceneBndLower), sceneBndUpper(sceneBndUpper) {}

	void move(glm::vec3 dir, float speed) {
		glm::vec3 collVec = glm::vec3(0.f);
		auto* testedNode = characterObject.nodes[0];
		for (const auto& obj : collisionObjects) {
			collVec += obj.getCollisionVec(characterObject, testedNode);
		}
		glm::vec3 moveVec = (dir - collVec) * speed;
		
		if (endlessMode) {
			moveVec.y = 0;
			glm::vec3 texTrans = movedNode->transformLocal * glm::vec4(moveVec, 0.f);
			//texTrans.y = 0;
			planeObj.uvOffset += glm::vec2(texTrans.x, texTrans.z) / 2.9f;
			//glm::vec3 worldTrans = glm::inverse(movedNode->transformLocal) * glm::vec4(moveVec, 1.f);
			worldMovedNode->transformLocal = glm::translate(worldMovedNode->transformLocal, -texTrans);
		}
		else {
			movedNode->transformLocal = glm::translate(movedNode->transformLocal, moveVec);
		}

		glm::vec3 pos = movedNode->getLocalPos();
		glm::vec3 npos = pos;
		glm::vec3 lower = sceneBndLower + characterObject.boundSphereRadius;
		glm::vec3 upper = sceneBndUpper - characterObject.boundSphereRadius;
		if (pos.y < lower.y) {
			npos.y = lower.y;
		}
		if (pos.x < lower.x) {
			npos.x = upper.x;
		}
		if (pos.x > upper.x) {
			npos.x = lower.x;
		}
		if (pos.z < lower.z) {
			npos.z = upper.z;
		}
		if (pos.z > upper.z) {
			npos.z = lower.z;
		}
		movedNode->setLocalPos(npos);
	}

	void startWalking() {
		characterObject.animationPaused = false;
	}

	void stopWalking() {
		characterObject.animationPaused = true;
	}

	void turnLeft(float speed) {
		glm::vec3 movedY = glm::normalize(glm::vec3(movedNode->transform[1]));
		movedNode->transformLocal = glm::rotate(movedNode->transformLocal, glm::half_pi<float>() * speed, movedY);
	}

	void turnRight(float speed) {
		glm::vec3 movedY = glm::normalize(glm::vec3(movedNode->transform[1]));
		movedNode->transformLocal = glm::rotate(movedNode->transformLocal, -glm::half_pi<float>() * speed, movedY);
	}
};
