#include "global.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include "render.h"

static double prevXPos, prevYPos;
bool prevPosValid = false;

static const float panSpeed = 2.0f;

class App {

	GLFWwindow *window;

	int winHeight;
	int winWidth;

	Render& render;

	void initWindow() {
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		window = glfwCreateWindow(winWidth, winHeight, winTitle.data(), nullptr, nullptr);
		if (window == nullptr) {
			glfwTerminate();
			throw std::runtime_error("Failed to create window");
		}
		glfwMakeContextCurrent(window);
		//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}

	void initGLAD() {
		if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
			throw std::runtime_error("Failed to init GLAD");
		}
	}

	void initOGL() {
		glViewport(0, 0, winWidth, winHeight);
		//glClearColor(0.2f, 0.6f, 0.2f, 1.0f);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_DEPTH_TEST);
		glCullFace(GL_BACK);
		//glEnable(GL_CULL_FACE);
		glEnable(GL_MULTISAMPLE);
		//glEnable(GL_FRAMEBUFFER_SRGB);
		glLineWidth(3.0f);
		glPointSize(5.0f);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glClearStencil(-1);
	}

	void registerCallbacks() {
		glfwSetFramebufferSizeCallback(window, resizedCallback);
		glfwSetCursorPosCallback(window, mouseMove);
		glfwSetScrollCallback(window, mouseScroll);
		glfwSetMouseButtonCallback(window, mouseClick);
		glfwSetKeyCallback(window, keyPressed);
	}

	static void resizedCallback(GLFWwindow* window, int width, int height) {
		glViewport(0, 0, width, height);
		instance->winHeight = height;
		instance->winWidth = width;
		instance->render.camera->fovXYRatio = width / (float)height;
		instance->render.windowExtent = glm::vec2(width, height);
	}

	static void keyPressed(GLFWwindow* window, int key, int scancode, int action, int mods) {
		static bool wasPressed = false;
		if (key == GLFW_KEY_E) {
			if (action == GLFW_PRESS && !wasPressed) {
				bool& endless = instance->render.characterAnimator->endlessMode;
				endless = !endless;
				wasPressed = true;
			}
			if (action == GLFW_RELEASE) {
				wasPressed = false;
			}
		}
	}

	void processInput() {
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
			glfwSetWindowShouldClose(window, true);
		}
		auto& cam = instance->render.camera;

		bool fast = glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS;
		float speed = fast ? 0.5f : 0.05f;
		bool moving = true;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS 
			|| glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			instance->render.characterAnimator->turnLeft(0.03f);
		}
		else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS
			|| glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			instance->render.characterAnimator->turnRight(0.03f);
		}
		else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS
			|| glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			instance->render.characterAnimator->move(glm::vec3(0.f, 0.f, 1.f), speed);
		}
		else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS
			|| glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			instance->render.characterAnimator->move(glm::vec3(0.f, 0.f, -1.f), speed);
		}
		else {
			moving = false;
			instance->render.characterAnimator->stopWalking();
		}
		if (moving) {
			instance->render.characterAnimator->startWalking();
		}
		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			if (moving) {
				instance->render.characterAnimator->move(glm::vec3(0.f, 1.f, 0.f), 0.5f);
			}
		}
		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS) {
			instance->render.useFPSCam = false;
		}
		if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS) {
			instance->render.useFPSCam = true;
		}
	}

	static void mouseClick(GLFWwindow* window, int button, int action, int mods) {
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
			instance->render.processClick(int(std::round(xpos)), instance->winHeight - int(std::round(ypos)));
		}
	}

	static void mouseMove(GLFWwindow* window, double xpos, double ypos) {
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) != GLFW_PRESS) {
			prevPosValid = false;
			return;
		}
		if (xpos < 0 || ypos < 0 || xpos > instance->winWidth || ypos > instance->winHeight) {
			return;
		}
		if (prevPosValid) {
			glm::vec2 delta = glm::vec2(xpos - prevXPos, ypos - prevYPos) / glm::vec2(instance->winWidth, instance->winHeight);
			if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
				instance->render.camera->moveByScreenNormDelta(delta * panSpeed);
			}
			else {
				instance->render.camera->rotateByScreenNormDelta(delta);
			}
		}
		prevXPos = xpos;
		prevYPos = ypos;
		prevPosValid = true;
	}

	static void mouseScroll(GLFWwindow* window, double xOffset, double yOffset) {
		instance->render.camera->mouseScroll(yOffset, glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS);
	}

	App(std::string title, int w, int h, Render& render)
		:winTitle(title), winWidth(w), winHeight(h), render(render) {

		initWindow();
		initGLAD();
		initOGL();
		registerCallbacks();

		render.windowExtent = glm::vec2(w, h);
	}

public:

	~App() {}

	static std::unique_ptr<App> instance;

	const std::string &winTitle;

	static App &create(std::string title, int w, int h, Render& render) {
		instance = std::unique_ptr<App>(new App(title, w, h, render));
		return *instance;
	}

	void startRender() {
		render.initAtGLContext();

		while (!glfwWindowShouldClose(window)) {
			processInput();

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			render.draw();

			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		glfwTerminate();
	}
};

std::unique_ptr<App> App::instance = std::unique_ptr<App>();

int main() {
	Render render;
	App &app = App::create("MyEngine", 1000, 1000, render);
	app.startRender();
	return 0;
}