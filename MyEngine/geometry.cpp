#include "geometry.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

namespace std {
	template<> struct hash<Vertex> {
		size_t operator()(const Vertex& vert) const noexcept {
			return hash<glm::vec3>()(vert.pos) ^ (hash<glm::vec3>()(vert.color) << 1);
		}
	};
}

bool Vertex::operator == (const Vertex& other) const {
	return pos == other.pos && color == other.color;
}

static const std::string modelFolder = "models";
static const std::string dumpFolder = "dumps";

static inline const std::string vertDumpPath(std::string& name) {
	return dumpFolder + "/" + name + "-vert.dump";
}

static inline const std::string indDumpPath(std::string& name) {
	return dumpFolder + "/" + name + "-ind.dump";
}

void GeometryObject<Vertex>::saveDump() {

	if (!std::filesystem::exists(dumpFolder)) {
		std::filesystem::create_directories(dumpFolder);
	}

	writeBinary(vertDumpPath(name),
		reinterpret_cast<const char*>(vertices.data()),
		vertices.size() * sizeof(Vertex));

	writeBinary(indDumpPath(name),
		reinterpret_cast<const char*>(indices.data()),
		indices.size() * sizeof(uint32_t));
}

bool GeometryObject<Vertex>::loadDump() {
	std::string dumpVert = vertDumpPath(name);
	std::string dumpInd = indDumpPath(name);

	if (!std::filesystem::exists(dumpVert) || !std::filesystem::exists(dumpInd)) {
		return false;
	}
	std::vector<char> vertBytes = readBinary(dumpVert);
	std::vector<char> indBytes = readBinary(dumpInd);
	vertices.resize(vertBytes.size() / sizeof(Vertex));
	indices.resize(indBytes.size() / sizeof(uint32_t));
	memcpy(vertices.data(), vertBytes.data(), vertBytes.size());
	memcpy(indices.data(), indBytes.data(), indBytes.size());
}

void GeometryObject<Vertex>::loadObj(bool indexed) {
	std::string modelPath = modelFolder + "/" + name + ".obj";
	if (!std::filesystem::exists(modelPath)) {
		std::cout << "Model .obj file cannot be found" << std::endl;
		return;
	}

	tinyobj::ObjReaderConfig readerConfig;
	readerConfig.mtl_search_path = modelFolder + "/";
	readerConfig.triangulate = true;

	tinyobj::ObjReader reader;

	if (!reader.ParseFromFile(modelPath, readerConfig)) {
		if (!reader.Error().empty()) {
			std::cout << reader.Error() << std::endl;
		}
		throw std::runtime_error("Failed to parse model file");
	}
	if (!reader.Warning().empty()) {
		std::cout << reader.Warning() << std::endl;
	}

	auto& attrib = reader.GetAttrib();
	auto& shapes = reader.GetShapes();

	std::unordered_map<Vertex, uint32_t> vertexInds;

	for (const auto& index : shapes[0].mesh.indices) {
		Vertex vertex{};
		size_t vind = index.vertex_index;
		memcpy(&vertex.pos, &attrib.vertices[3 * vind], 3 * sizeof(float));
		memcpy(&vertex.color, &attrib.colors[3 * vind], 3 * sizeof(float));
		memcpy(&vertex.normal, &attrib.normals[3 * (size_t)index.normal_index], 3 * sizeof(float));
		if (index.texcoord_index != -1) {
			memcpy(&vertex.texCoord, &attrib.texcoords[2 * (size_t)index.texcoord_index], 2 * sizeof(float));
		}

		if (indexed) {
			if (vertexInds.find(vertex) == vertexInds.end()) {
				vertexInds[vertex] = vertices.size();
				vertices.push_back(vertex);
			}
			indices.push_back(vertexInds[vertex]);
		}
		else {
			vertices.push_back(vertex);
			indices.push_back(vind);
		}
	}
	auto& mats = reader.GetMaterials();
	if (!mats.empty()) {
		if (!mats[0].diffuse_texname.empty()){
			material.diffuseTexture = std::make_unique<Texture>(mats[0].diffuse_texname);
		}
		if (!mats[0].specular_texname.empty()) {
			material.specularTexture = std::make_unique<Texture>(mats[0].specular_texname);
		}
	}
}

glm::vec2 cubeUV(glm::vec3 pos, glm::vec3& norm, const glm::vec2 uvs[3]){
	struct side {
		int j;
		int i;
	};
	std::vector<side> sides = { {1, 0}, {0, 1}, {1, 1}, {2, 1}, {3, 1}, {1, 2} };
	int sideId;
	glm::vec2 uv;
	if (norm.x != 0) {
		sideId = norm.x > 0 ? 3 : 1;
		uv = uvs[0];
	}
	if (norm.y != 0) {
		sideId = norm.y > 0 ? 0 : 5;
		uv = uvs[1];
	}
	if (norm.z != 0) {
		sideId = norm.z > 0 ? 4 : 2;
		uv = uvs[2];
	}
	side s = sides[sideId];
	glm::vec2 split = glm::vec2(1/4.f, 1 / 3.f);
	glm::vec2 res = glm::vec2(s.j, s.i) * split + (glm::vec2(uv.x, uv.y) / 2.f + .5f) * split;
	return res;//glm::vec2(res.y, res.x);
}

GeometryObject<Vertex>* GeometryObject<Vertex>::makeCube() {
	auto cube = new GeometryObject<Vertex> ();
	struct VertexData {
		glm::vec3 pos;
		glm::vec2 uvs[3];
	};
	std::vector<VertexData> vertices = {
		{{1.0f, 1.0f, 1.0f}, {{1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}}},
		{{1.0f, 1.0f, -1.0f}, {{0.f, 0.f}, {1.f, 1.f}, {0.f, 1.f}}},
		{{1.0f, -1.0f, 1.0f}, {{1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}}},
		{{1.0f, -1.0f, -1.0f}, {{1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}}},
		{{-1.0f, 1.0f, 1.0f}, {{1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}}},
		{{-1.0f, 1.0f, -1.0f}, {{1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}}},
		{{-1.0f, -1.0f, 1.0f}, {{1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}}},
		{{-1.0f, -1.0f, -1.0f}, {{1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}}}
	};
	for (const auto &vertex : vertices) {
		glm::vec3 n1 = { glm::sign(vertex.pos.x), 0.0f, 0.0f };
		glm::vec3 n2 = { 0.0f, glm::sign(vertex.pos.y), 0.0f };
		glm::vec3 n3 = { 0.0f, 0.0f, glm::sign(vertex.pos.z) };
		cube->vertices.push_back({ vertex.pos, n1, {}, cubeUV(vertex.pos, n1, vertex.uvs) });
		cube->vertices.push_back({ vertex.pos, n2, {}, cubeUV(vertex.pos, n2, vertex.uvs) });
		cube->vertices.push_back({ vertex.pos, n3, {}, cubeUV(vertex.pos, n3, vertex.uvs) });
	}
	cube->indices.resize(36);
	for (int i = 0; i < 6; i++) {
		GLuint side[4];
		int cnt = 0;
		for (int j = 0; j < cube->vertices.size(); j++) {
			int coord = glm::value_ptr(cube->vertices[j].normal)[(int)(i / 2)];
			if (coord != 0 && ((coord == 1) == i % 2)) {
				side[cnt] = j;
				cnt++;
			}
		}
		memcpy(cube->indices.data() + i * 6, side, sizeof(GLuint) * 3);
		memcpy(cube->indices.data() + i * 6 + 3, side + 1, sizeof(GLuint) * 3);
	}
	return cube;
}

GeometryObject<Vertex>* GeometryObject<Vertex>::makeUVSphere(float radius, int steps) {
	auto* uvSphere = new GeometryObject();
	//poles
	uvSphere->vertices.push_back({ {0.0f, radius, 0.0f}, {0.0f, 1.0f, 0.0f} });
	uvSphere->vertices.push_back({ {0.0f, -radius, 0.0f}, {0.0f, -1.0f, 0.0f} });
	//TODO
	return uvSphere;
}

GeometryObject<Vertex>* GeometryObject<Vertex>::makeTorus(float radius, float ringRadius, int uSteps, int vSteps) {
	auto* torus = new GeometryObject();
	float uStride = glm::two_pi<float>() / uSteps;
	float vStride = glm::two_pi<float>() / vSteps;
	for (int i = 0; i <= uSteps; i++) {
		float u = i * uStride;
		glm::vec3 ringOrigin = glm::vec3{ glm::cos(u), glm::sin(u), 0 } *radius;
		for (int j = 0; j <= vSteps; j++) {
			float v = j * vStride;
			glm::vec3 normal = glm::vec3{ glm::cos(u) * glm::cos(v), glm::sin(u) * glm::cos(v), -glm::sin(v) };
			Vertex vert{ normal * ringRadius + ringOrigin, normal };
			vert.texCoord.s = (float)i / (uSteps);
			vert.texCoord.t = (float)j / (vSteps);
			torus->vertices.push_back(vert);

			if (i == uSteps || j == vSteps) continue;

			int i_next = (i + 1);
			int j_next = (j + 1);
			int ringNum = vSteps + 1;
			int v1 = i * ringNum + j;
			int v2 = i * ringNum + j_next;
			int v3 = i_next * ringNum + j;
			int v4 = i_next * ringNum + j_next;

			torus->indices.push_back(v1);
			torus->indices.push_back(v2);
			torus->indices.push_back(v4);

			torus->indices.push_back(v1);
			torus->indices.push_back(v4);
			torus->indices.push_back(v3);
		}
	}
	return torus;
}

GeometryObject<Vertex>* GeometryObject<Vertex>::makePlane(int xTiles, int yTiles, float tileSide, glm::vec2 uvScale, glm::vec2 uvOffset) {
	auto* plane = new GeometryObject;
	float midX = xTiles * tileSide * 0.5;
	float midY = yTiles * tileSide * 0.5;
	struct VertData {
		glm::vec2 pos;
		glm::vec2 uv;
	};
	for (int i = 0; i < xTiles; ++i) {
		float xOffset = i * tileSide - midX;
		for (int j = 0; j < yTiles; ++j) {
			float yOffset = j * tileSide - midY;

			std::vector<VertData> verts = {
				{{0.0f, 1.0f}, {0.0f, 1.0f}},
				{{1.0f, 1.0f}, {1.0f, 1.0f}},
				{{1.0f, 0.0f}, {1.0f, 0.0f}},
				{{0.0f, 0.0f}, {0.0f, 0.0f}}
			};
			std::vector<uint32_t> inds = { 0, 3, 1, 1, 3, 2 };
			for (VertData vert : verts) {
				glm::vec3 pos = glm::vec3(vert.pos.x * tileSide + xOffset, 0.0f, vert.pos.y * tileSide + yOffset);
				plane->vertices.push_back(
					{ pos,
					glm::vec3(0.0f, 1.0f, 0.0f),
					glm::vec3(1.0f, 0.0f, 0.0f),
					uvOffset + vert.uv * uvScale});
			}
			auto indOffset = verts.size() * (i * yTiles + j);
			for (auto ind : inds) {
				plane->indices.push_back(ind + indOffset);
			}
		}
	}
	return plane;
}

GeometryObject<PGRVertex>* GeometryObject<PGRVertex>::loadWizard() {
	auto* geom = new GeometryObject<PGRVertex>();
	std::vector<const float*> frames = {
		bodyVertices, body_001Vertices, body_002Vertices, body_003Vertices, body_004Vertices, body_005Vertices,
		bodyVertices, body_001Vertices
	};
	geom->vertices.resize(bodyNVertices * frames.size());
	for (size_t i = 0; i < frames.size(); ++i) {
		const float* vertices = frames[i];
		memcpy(geom->vertices.data() + i * bodyNVertices, vertices, sizeof(bodyVertices));
	}
	geom->indices.resize(bodyNTriangles * 3);
	memcpy(geom->indices.data(), bodyTriangles, sizeof(bodyTriangles));
	geom->numFrames = frames.size();
	geom->material.diffuseTexture = std::make_unique<Texture>("models/evil/texture.png");
	return geom;
}

GeometryObject<PGRVertex>* GeometryObject<PGRVertex>::loadCube() {
	auto* geom = new GeometryObject<PGRVertex>();
	geom->vertices.resize(cubeNVertices);
	memcpy(geom->vertices.data(), cubeVertices, sizeof(cubeVertices));
	geom->indices.resize(cubeNTriangles * 3);
	memcpy(geom->indices.data(), cubeTriangles, sizeof(cubeTriangles));
	return geom;
}
