#include "util.h"

std::string readText(std::string fname) {
	std::ifstream fstream(fname, std::ios::in);
	if (!fstream.is_open()) {
		throw std::runtime_error("Failed to open shader file " + fname);
	}
	return std::string(std::istreambuf_iterator<char>(fstream), std::istreambuf_iterator<char>());
}

std::vector<char> readBinary(const std::string& filename) {
	std::ifstream file(filename, std::ios::binary | std::ios::ate);
	if (!file.is_open()) {
		throw std::runtime_error("Failed to open binary file " + filename);
	}
	size_t fsize = file.tellg();
	file.seekg(0);
	std::vector<char> bytes(fsize);
	file.read(bytes.data(), fsize);
	file.close();
	return bytes;
}

void writeBinary(const std::string& filename, const char* data, size_t size) {
	std::ofstream file(filename, std::ios::binary);
	if (!file.is_open()) {
		throw std::runtime_error("Failed to open binary file " + filename);
	}
	file.write(data, size);
	file.close();
}