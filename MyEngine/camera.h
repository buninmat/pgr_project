#pragma once
#include "global.h"
#include "scene_graph.h"

struct Camera
{
	float fovY = 45.0f;
	float fovXYRatio = 1.0f;
	float near = 0.1f;
	float far = 10.0f;

	SceneNode* node;

	glm::mat4 getProj() {
		return glm::perspective(glm::radians(fovY), fovXYRatio, near, far);
	}
	virtual void rotateByScreenNormDelta(glm::vec2 delta) = 0;

	virtual void moveByScreenNormDelta(glm::vec2 delta) = 0;

	virtual void moveCamSpace(glm::vec3 delta) = 0;

	virtual glm::mat4 getView() = 0;

	virtual void mouseScroll(float value, bool toggle) {};

	virtual void updateTransform() = 0;

	virtual glm::mat3 getLinBase() = 0;

};

struct EulerCamera : Camera {

	float yaw = 0.0f;
	float pitch = 0.0f;

	void rotateByScreenNormDelta(glm::vec2 delta) override;

	virtual void moveByScreenNormDelta(glm::vec2 delta) override {};

	glm::mat3 getLinBase() override;

	glm::mat4 getView() override;

	void moveCamSpace(glm::vec3 delta) override {}

	void updateTransform() override {}
};

struct OrbitCamera : EulerCamera {

	float orbitDistance = 1.0f;

	glm::vec2 offset{};

	glm::mat4 getView() override;

	void mouseScroll(float offset, bool toggle) override;

	void rotateByScreenNormDelta(glm::vec2 delta) override;

	void moveByScreenNormDelta(glm::vec2 delta) override {
		offset += delta;
	}

	void moveCamSpace(glm::vec3 delta) override;

	void updateTransform() override;
};

