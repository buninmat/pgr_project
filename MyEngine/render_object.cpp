#include "render_object.h"

static int roInstanceCounter = 0;

RenderObject::RenderObject(const GeometryObject<Vertex>& geom, const ShaderProgram& shader)
	:shader(shader), numIndices(geom.indices.size()), lightSources(lightSources), matParams(geom.material.params) {
	fillElementArrays(geom);
	std::cout << glGetError() << std::endl;
	createTextures(geom.material);
	id = roInstanceCounter++;
	std::cout << glGetError() << std::endl;
}

RenderObject::RenderObject(const GeometryObject<PGRVertex>& geom, const ShaderProgram& shader)
	: shader(shader), numIndices(geom.indices.size()), numFrames(geom.numFrames)
	, lightSources(lightSources), matParams(geom.material.params) {
	fillElementArrays(geom);
	createTextures(geom.material);
	id = roInstanceCounter++;
	std::cout << glGetError() << std::endl;
}

void RenderObject::createTextures(const Material& material) {
	if (material.diffuseTexture != nullptr) {
		matParams.diffuse = glm::vec4(-1.f);
		diffuseTexture = material.diffuseTexture->createOGLObj();
	}
	if (material.specularTexture != nullptr) {
		matParams.specular = glm::vec4(-1.f);
		specularTexture = material.specularTexture->createOGLObj();
	}
}


RenderObject::~RenderObject() {
	glDeleteVertexArrays(1, &vertexArray);
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &elementBuffer);
	glDeleteTextures(1, &diffuseTexture);
	glDeleteTextures(1, &specularTexture);
}

template<class VertexT>
void RenderObject::fillElementArrays(const GeometryObject<VertexT>& geom) {

	glGenVertexArrays(1, &vertexArray);
	glBindVertexArray(vertexArray);

	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom.vertices.size() * sizeof(VertexT), geom.vertices.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &elementBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(GLuint), geom.indices.data(), GL_STATIC_DRAW);

	GLint pos_loc = 0;
	glEnableVertexAttribArray(pos_loc);
	glVertexAttribPointer(pos_loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexT), (void*)offsetof(VertexT, pos));

	GLint normal_loc = 1;
	glEnableVertexAttribArray(normal_loc);
	glVertexAttribPointer(normal_loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexT), (void*)offsetof(VertexT, normal));

	if (true) {
		GLint tex = 2;
		glEnableVertexAttribArray(tex);
		glVertexAttribPointer(tex, 3, GL_FLOAT, GL_FALSE, sizeof(VertexT), (void*)offsetof(VertexT, texCoord));
	}
	else {
		GLint color_loc = 2;
		glEnableVertexAttribArray(color_loc);
		glVertexAttribPointer(color_loc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, color));
	}

	if (numFrames > 1) {
		frameOffset = (geom.vertices.size() / numFrames);

		GLint nextPosLoc = 3;
		glEnableVertexAttribArray(nextPosLoc);
		glVertexAttribPointer(nextPosLoc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexT),
			(void*)(frameOffset * sizeof(VertexT) + offsetof(VertexT, pos)));

		GLint nextNormLoc = 4;
		glEnableVertexAttribArray(nextNormLoc);
		glVertexAttribPointer(nextNormLoc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexT),
			(void*)(frameOffset * sizeof(VertexT) + offsetof(VertexT, normal)));

		GLint nextNextPosLoc = 5;
		glEnableVertexAttribArray(nextNextPosLoc);
		glVertexAttribPointer(nextNextPosLoc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexT),
			(void*)(2 * frameOffset * sizeof(VertexT) + offsetof(VertexT, pos)));

		GLint nextNextNormLoc = 6;
		glEnableVertexAttribArray(nextNextNormLoc);
		glVertexAttribPointer(nextNextNormLoc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexT),
			(void*)(2 * frameOffset * sizeof(VertexT) + offsetof(VertexT, normal)));
	}

	glBindVertexArray(0);
}

void RenderObject::render(Camera& camera, GLuint lightsUBO, GLuint fogTexture, glm::vec2 windowExt, int frameInd, float frameTime) {
	glUseProgram(shader.program);

	glBindBuffer(GL_UNIFORM_BUFFER, lightsUBO);

	bool animated = numFrames > 1;
	if (animated) {
		glUniform1f(shader.uniform("mixT"), frameTime);
	}
	glUniform1i(shader.uniform("lighted"), lighted);
	glUniform4fv(shader.uniform("globalAmbient"), 1, glm::value_ptr(globalAmbient));

	glUniform4fv(shader.uniform("material.diffuse"), 1, glm::value_ptr(matParams.diffuse));
	glUniform4fv(shader.uniform("material.specular"), 1, glm::value_ptr(matParams.specular));
	glUniform4fv(shader.uniform("material.ambient"), 1, glm::value_ptr(matParams.ambient));
	glUniform4fv(shader.uniform("material.emissive"), 1, glm::value_ptr(matParams.emissive));
	glUniform1f(shader.uniform("material.sharpness"), matParams.sharpness);

	auto view = camera.getView();
	glUniformMatrix4fv(shader.uniform("transform.view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(shader.uniform("transform.projection"), 1, GL_FALSE, glm::value_ptr(camera.getProj()));
	auto viewer = glm::vec3(glm::inverse(view)[3]);
	glUniform3fv(shader.uniform("viewer"), 1, glm::value_ptr(viewer));

	glm::vec2 texOffset = glm::vec2(0.f);
	if (numTexFrames != 0) {
		texOffset = uvOffset * float(frameInd % (numTexFrames - 1));
	}
	else {
		texOffset += uvOffset;
	}
	glUniform2fv(shader.uniform("uvOffset"), 1, glm::value_ptr(texOffset));

	glUniform2fv(shader.uniform("windowExtent"), 1, glm::value_ptr(windowExt));

	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	for (const auto& node : nodes) {
		glStencilFunc(GL_ALWAYS, node->objectId + 1, 0);

		glUniformMatrix4fv(shader.uniform("transform.model"), 1, GL_FALSE, glm::value_ptr(node->transform));

		glPolygonMode(GL_FRONT_AND_BACK, renderMode);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, diffuseTexture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, specularTexture);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, fogTexture);

		glUniform1i(shader.uniform("diffuseTexture"), 0);
		glUniform1i(shader.uniform("specularTexture"), 1);
		glUniform1i(shader.uniform("fogTexture"), 2);

		glBindVertexArray(vertexArray);
		static int offset = 0;
		static bool first = true;
		if (animated) {
			// finish the animation before stopping it
			uint32_t animFrameId = std::max(frameInd - animStartFrameInd + offset, 0) % (numFrames - 2);
			bool stopped = false;
			if (animationPaused && animFrameId == 0) {
				animStartFrameInd = frameInd + 1;
				stopped = true;
				offset = 0;
				first = true;
			}
			bool useNextNext = false;
			if (!animationPaused) {
				if (animFrameId == numFrames - 3) {
					useNextNext = true;
				}
				else {
					if (animFrameId == 0) {
						if (!first) {
							animFrameId = 1;
							offset += 1;
						}
					}
					else {
						first = false;
					}
				}
				std::cout << animFrameId << std::endl;
			}
			glUniform1i(shader.uniform("stopped"), stopped);
			glUniform1i(shader.uniform("useNextNext"), useNextNext);
			glDrawElementsBaseVertex(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0, frameOffset * animFrameId);
		}
		else {
			glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
		}
	}

	glDisable(GL_STENCIL_TEST);

	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}

glm::vec4 RenderObject::globalAmbient = glm::vec4(0.0);

glm::vec3 RenderObject::getCollisionVec(RenderObject& tested, SceneNode* testedNode) const {
	glm::vec3 colSum = glm::vec3(0.f);
	if (id == tested.id || tested.collisionDisabled || collisionDisabled || testedNode->disabled) {
		return colSum;
	}
	for (const auto& node : nodes) {
		glm::vec3 colVec = node->getGlobalPos() - testedNode->getGlobalPos();
		float dist = glm::length(colVec);
		if (dist > 0 && dist < boundSphereRadius + tested.boundSphereRadius) {
			colSum += colVec / dist;
		}
	}
	float len = glm::length(colSum);
	if (len > 0) {
		colSum /= len;
	}
	return colSum;
}

