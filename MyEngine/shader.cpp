#include "shader.h"

ShaderProgram::ShaderProgram(std::string fileVertex, std::string fileFragment) {
	GLuint vertexShader = compileShader(fileVertex, GL_VERTEX_SHADER);
	GLuint fragmentShader = compileShader(fileFragment, GL_FRAGMENT_SHADER);
	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);
	int success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		char linkInfo[512];
		glGetProgramInfoLog(program, sizeof(linkInfo), nullptr, linkInfo);
		std::cout << "Failed to link shader program\n" << linkInfo << std::endl;
		throw std::runtime_error("Failed to link shaders");
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

GLuint ShaderProgram::compileShader(std::string file, GLenum stage) {
	GLuint shader = glCreateShader(stage);
	std::string shaderStr = readText(file);
	const char* shaderText = shaderStr.data();
	glShaderSource(shader, 1, &shaderText, nullptr);
	glCompileShader(shader);
	int success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		char compileInfo[512];
		glGetShaderInfoLog(shader, sizeof(compileInfo), nullptr, compileInfo);
		std::cout << "Failed to compile" + file << std::endl << compileInfo << std::endl;
		throw std::runtime_error("Shader compilation failed");
	}
	return shader;
}