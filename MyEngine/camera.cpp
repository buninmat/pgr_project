#include "camera.h"

static inline float fitAngle(float angle) {
	int fullDeg = 360 * (int)(angle / 360);
	if (angle >= 360) {
		return angle - fullDeg;
	}
	if (angle < 0) {
		return angle + fullDeg + 360;
	}
	return angle;
}

void EulerCamera::rotateByScreenNormDelta(glm::vec2 delta) {
	float fovX = fovXYRatio * fovY;
	yaw = fitAngle(yaw + fovX * delta.x);
	pitch = fitAngle(pitch + fovY * delta.y);
}

glm::mat4 EulerCamera::getView() {
	float y = glm::radians(yaw);
	float p = glm::radians(pitch);
	glm::vec3 translation = node->transform * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	glm::vec3 center = translation + glm::vec3(glm::rotate(glm::mat4(1.f), 3.5f*glm::half_pi<float>(), glm::vec3(0.f, 1.f, 0.f)) * ((node->transform[0]) + (node->transform[2])));
	//glm::vec3(glm::sin(y) * glm::cos(p), -glm::sin(p), glm::cos(p) * glm::cos(y));
	glm::vec3 up = glm::vec3(glm::sin(y) * glm::sin(p), glm::cos(p), glm::sin(p) * glm::cos(y));
	return glm::lookAt(translation, center, up);
}

glm::mat3 EulerCamera::getLinBase() {
	glm::mat3 rot;
	float y = glm::radians(yaw);
	float p = glm::radians(pitch);

	rot[1] = glm::vec3(glm::sin(y) * glm::sin(p), glm::cos(p), glm::sin(p) * glm::cos(y)); //y
	rot[2] = glm::vec3(glm::sin(y) * glm::cos(p), -glm::sin(p), glm::cos(p) * glm::cos(y)); //z
	rot[0] = glm::cross(rot[2], rot[1]); // x

	return rot;
}


void OrbitCamera::mouseScroll(float offset, bool toggle) {
	if (toggle) {
		fovY -= offset;
		if (fovY <= 1.0f) {
			fovY = 1.0f;
		}
		if (fovY >= 45.0f) {
			fovY = 45.0f;
		}
	}
	else {
		orbitDistance += offset;
		updateTransform();
	}
}

void OrbitCamera::rotateByScreenNormDelta(glm::vec2 delta) {
	float fovX = fovXYRatio * fovY;
	yaw = fitAngle(yaw + 180.f * delta.x);
	pitch = fitAngle(pitch + 180.f * delta.y);
	updateTransform();
}

glm::mat4 OrbitCamera::getView() {
	glm::vec3 translation = node->transform[3];
	glm::mat3 rot = getLinBase();
	glm::vec3& camY = rot[1];
	glm::vec3& camZ = rot[2];
	glm::vec3 camOffset = rot[0] * offset.x + camY * offset.y;
	glm::vec3 translate = translation + orbitDistance * camZ + camOffset;
	translation += camOffset;
	return glm::lookAt(translate, translation, camY);
}

void OrbitCamera::moveCamSpace(glm::vec3 delta) {
	node->transformLocal = glm::translate(node->transformLocal, getLinBase() * delta);
}

void OrbitCamera::updateTransform() {
	glm::mat4 linBase = getLinBase();
	linBase[3] = node->transformLocal[3];
	node->transformLocal = linBase;
}





