#pragma once
#include "global.h"

struct Texture {

	unsigned char* data;
	int width;
	int height;
	int nChannels;

	Texture(Texture &other);

	Texture(std::string file);

	~Texture();

	static Texture* makeSquare();

	static Texture* makeText(std::stringstream& text, int width, int height);

	GLuint createOGLObj();

private:
	bool copied = false;
	bool stbLoaded = false;

	Texture() {}
};
