#pragma once
#include "render_object.h"

class NodeAnimator {
	SceneNode* node;
	float duration;
	float magnitude;
	std::chrono::high_resolution_clock::time_point startTime;

	glm::vec3 getPos(float t) {
		t *= glm::two_pi<float>();
		return glm::vec3(glm::cos(t), 0, glm::sin(t)) * magnitude;
	}

	glm::vec3 getDerivative(float t) {
		t *= glm::two_pi<float>();
		return glm::vec3(-glm::sin(t), 0, glm::cos(t));
	}

public:

	NodeAnimator(SceneNode* node, float duration, float magnitude)
		: node(node), duration(duration), magnitude(magnitude) {}

	void start() {
		startTime = std::chrono::high_resolution_clock::now();
	}

	void update() {
		auto end = std::chrono::high_resolution_clock::now();
		float time = std::chrono::duration<float, std::chrono::milliseconds::period>(end - startTime).count();
		if (time > duration) {
			startTime = std::chrono::high_resolution_clock::now();
			time = 0;
		}
		float t = time / duration;
		node->transformLocal[3] = glm::vec4(getPos(t), 1.f);
		glm::vec3 der = getDerivative(t);
		glm::vec3 up = glm::vec3(0., 1., 0.);
		glm::vec3 side = glm::cross(up, der);
		node->transformLocal[0] = glm::vec4(der, 0.f);
		node->transformLocal[2] = glm::vec4(side, 0.f);
		node->transformLocal[1] = glm::vec4(glm::cross(side, der), 0.f);
	}
};