#pragma once
#include "global.h"
#include "util.h"

class ShaderProgram {

	GLuint compileShader(std::string file, GLenum stage);

public:
	GLuint program{};

	ShaderProgram() {}

	ShaderProgram(std::string fileVertex, std::string fileFragment);

	inline GLint attrib(const char* name) {
		return glGetAttribLocation(program, name);
	}

	inline GLint uniform(const char* name) {
		return glGetUniformLocation(program, name);
	}

	inline GLint uniformBlock(const char* name) {
		return glGetUniformBlockIndex(program, name);
	}

	void setBinding(GLint uniformBlock, GLuint binding) {
		glUniformBlockBinding(program, uniformBlock, binding);
	}
};

