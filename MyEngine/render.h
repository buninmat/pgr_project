#pragma once
#include "render_object.h"
#include "node_animator.h"
#include "character_animator.h"

/// <summary>
/// Contains scene parameters and renders the scene
/// </summary>

class Render
{
	ShaderProgram phongShader;

	ShaderProgram colorShader;
	ShaderProgram animShader;

	std::unique_ptr<SceneNode> sceneRoot;

	std::list<RenderObject> renderObjects;

	std::vector<std::unique_ptr<LightSource>> lightSources;

	std::vector<LightSourceData> lightSourcesData;

	std::vector<NodeAnimator> nodeAnimators;

	unsigned long frameInd = 0;

	GLuint lightsUBO;

	const float animFramePeriodSec = 200;

	const glm::vec4 globalAmbient = glm::vec4(0.1, 0.1, 0.1, 1);

	std::vector<SceneNode*> *clickableNodes;

	GLuint fogTexture;

	int planeId;

public:

	bool useFPSCam = false;

	glm::vec2 windowExtent;

	std::unique_ptr<Camera> camera;

	std::unique_ptr<Camera> fpsCamera;
	
	std::unique_ptr<CharacterAnimator> characterAnimator;

	Render() {
		RenderObject::globalAmbient = globalAmbient;
	}

	void updateLightSources();

	void bindLightsUBO();

	void processClick(int x, int y);

	void draw();

	/// <summary>
	/// 
	/// Configure the scene here
	/// 
	/// </summary>
	void initAtGLContext() {

		// NODES ------------------------------------

		sceneRoot = std::make_unique<SceneNode>();

		auto* worldMovedNode = &sceneRoot->addChild();

		auto& ufoBase = worldMovedNode->addChild();
		ufoBase.transformLocal = glm::translate(ufoBase.transformLocal, glm::vec3(0.f, 0.f, -5.f));
		std::vector<SceneNode*> ufoAnimNodes;
		std::vector<SceneNode*> ufoModelNodes;
		for (int i = 0; i < 5; ++i) {
			auto* parent = i == 0 ? &ufoBase : ufoAnimNodes[i - 1];

			auto* node = &parent->addChild();
			node->transformLocal = glm::translate(glm::mat4(1.0f), glm::vec3(.0f, 1.5f * (i % 2 == 0? 1 : -1), .0f));

			auto* animNode = &node->addChild();
			ufoAnimNodes.push_back(animNode);
			nodeAnimators.emplace_back(animNode, 2000.f / (i + 1), 1.f);

			auto* modelNode = &animNode->addChild();
			ufoModelNodes.push_back(modelNode);
			modelNode->transformLocal = glm::scale(modelNode->transformLocal, glm::vec3(glm::pow(0.8f, i)));
			modelNode->transformLocal = glm::rotate(modelNode->transformLocal, glm::half_pi<float>(), glm::vec3(1.0f, 0.0f, 0.0f));
		}

		// Trash cans with lights
		std::vector<SceneNode*> trashCanNodes;
		std::default_random_engine generatorX(0);
		std::default_random_engine generatorY(1);
		std::uniform_real_distribution<float> distribution(-150.f, 150.f);
		auto randX = std::bind(distribution, generatorX);
		auto randY = std::bind(distribution, generatorY);
		LightColor trashLightColor;
		trashLightColor.diffuse = glm::vec4(0.f, 1.f, 0.f, 0.f);
		auto& trashLight = lightSources.emplace_back(new PointLight(trashLightColor));
		trashLight->intensity = 5;
		for (int i = 0; i < 50; i++) {
			auto& node = worldMovedNode->addChild();
			node.objectId = i;
			trashCanNodes.push_back(&node);
			node.transformLocal = glm::translate(glm::mat4(1.f), glm::vec3(randX(), 1.f, randY()));
			auto& lightNode = node.addChild();
			lightNode.transformLocal = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 1.f, 0.f));
			trashLight->nodes.push_back(&lightNode);
		}

		clickableNodes = &trashLight->nodes;

		auto& movedNode = sceneRoot->addChild();
		movedNode.transformLocal = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.1f, 2.0f));

		auto& wizardNode = movedNode.addChild();
		wizardNode.transformLocal = glm::scale(wizardNode.transformLocal, glm::vec3(0.2f, 0.2f, 0.2f));
		wizardNode.transformLocal = glm::rotate(wizardNode.transformLocal, -glm::half_pi<float>(), glm::vec3(1.0f, 0.0f, 0.0f));
		wizardNode.transformLocal = glm::translate(wizardNode.transformLocal, glm::vec3(0.0f, 0.f, -5.0f));

		auto& camNode = movedNode.addChild();

		auto& fpsCamNode = movedNode.addChild();
		fpsCamNode.transformLocal = glm::translate(fpsCamNode.transformLocal, glm::vec3(0.f, 1.f, 0.f));

		auto& skyboxNode = camNode.addChild();
		skyboxNode.transformLocal = glm::scale(glm::mat4(1.0f), glm::vec3(200.f));
		skyboxNode.cancelParentRotation = true;

		auto& sunNode = camNode.addChild();
		sunNode.cancelParentRotation = true;
		sunNode.transformLocal = glm::translate(sunNode.transformLocal, glm::vec3(0.f, 0.1f, 1.f) * 150.f);
		sunNode.transformLocal = glm::rotate(sunNode.transformLocal, glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
		sunNode.transformLocal = glm::scale(sunNode.transformLocal, glm::vec3(2.5f, 2.f, 2.f));

		auto& buddhaNode = worldMovedNode->addChild();
		buddhaNode.transformLocal = glm::translate(buddhaNode.transformLocal, glm::vec3(0.f, 1.f, 10.f));
		buddhaNode.transformLocal = glm::scale(buddhaNode.transformLocal, glm::vec3(2.f, 2.f, 2.f));

		auto& textNode = buddhaNode.addChild();
		textNode.transformLocal = glm::translate(textNode.transformLocal, glm::vec3(-5.5f, 1.0f, 0.f));
		textNode.transformLocal = glm::rotate(textNode.transformLocal, glm::half_pi<float>() * 3, glm::vec3(1.f, 0.f, 0.f));
		textNode.transformLocal = glm::rotate(textNode.transformLocal, glm::pi<float>(), glm::vec3(0.f, 0.f, 1.f));
		textNode.transformLocal = glm::scale(textNode.transformLocal, glm::vec3(3.f, 0.1f, 0.3f) * 0.5f);

		// skybox planes nodes
		auto& nd_sx = skyboxNode.addChild();
		nd_sx.transformLocal = glm::rotate(nd_sx.transformLocal, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
		nd_sx.transformLocal = glm::translate(nd_sx.transformLocal, glm::vec3(0.f, 1.f, 0.f));
		auto& nd_smz = nd_sx.addChild();
		nd_smz.transformLocal = glm::rotate(nd_smz.transformLocal, glm::half_pi<float>(), glm::vec3(0.f, 0.f, 1.f));
		nd_smz.transformLocal = glm::translate(nd_smz.transformLocal, glm::vec3(-1.f, 1.f, 0.f));
		auto& nd_sz = nd_sx.addChild();
		nd_sz.transformLocal = glm::rotate(nd_sz.transformLocal, -glm::half_pi<float>(), glm::vec3(0.f, 0.f, 1.f));
		nd_sz.transformLocal = glm::translate(nd_sz.transformLocal, glm::vec3(1.f, 1.f, 0.f));
		auto& nd_sy = nd_sx.addChild();
		nd_sy.transformLocal = glm::rotate(nd_sy.transformLocal, glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
		nd_sy.transformLocal = glm::translate(nd_sy.transformLocal, glm::vec3(0.f, 1.f, 1.f));
		auto& nd_smx = nd_sz.addChild();
		nd_smx.transformLocal = glm::rotate(nd_smx.transformLocal, -glm::half_pi<float>(), glm::vec3(0.f, 0.f, 1.f));
		nd_smx.transformLocal = glm::translate(nd_smx.transformLocal, glm::vec3(1.f, 1.f, 0.f));
		auto& nd_smy = nd_sx.addChild();
		nd_smy.transformLocal = glm::rotate(nd_smy.transformLocal, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
		nd_smy.transformLocal = glm::translate(nd_smy.transformLocal, glm::vec3(0.f, 1.f, -1.f));

		// GEOMETRIES ----------------------------------

		GeometryObject<Vertex> geometryBuddha("buddha", true);
		GeometryObject<Vertex> geomTrashCan("trash_can");
		GeometryObject<Vertex> geometrySphere("cube");
		geometrySphere.material.diffuseTexture = std::make_unique<Texture>("textures/cubemap.jpg");

		auto& geometryTorus = *GeometryObject<Vertex>::makeTorus(0.4f, 0.2f, 40, 30);
		geometryTorus.material.diffuseTexture = std::make_unique<Texture>("textures/metal-texture.jpg");
		//auto& geometryPlane = *GeometryObject<Vertex>::makePlane(150, 150, 2.f);
		auto& geometryPlane = *GeometryObject<Vertex>::makePlane(1, 1, 300.f, glm::vec2(100.f, 100.0f));
		geometryPlane.material.params.emissive = glm::vec4(0.1, 0.1, 0.0, 1.0f);
		geometryPlane.material.diffuseTexture = std::unique_ptr<Texture>(Texture::makeSquare());

		auto& wizard = *GeometryObject<PGRVertex>::loadWizard();

		auto& geomSun = *GeometryObject<Vertex>::makePlane(1, 1, 10.f, glm::vec2(0.2f, 1.f));
		geomSun.material.diffuseTexture = std::make_unique<Texture>("textures/sun.png");

		auto& geomText = *GeometryObject<Vertex>::makePlane(1, 1, 10.f);
		std::stringstream text;
		text << "PLEASE DON`T PANIC" << std::endl;
		geomText.material.diffuseTexture = std::unique_ptr<Texture>(Texture::makeText(text, 500, 50));

		//skybox planes
		/*Texture skyboxTex("textures/cubemap.jpg");
		auto& sy = *GeometryObject<Vertex>::makePlane(1, 1, 2, { 1.f, 1.f }, {0.25f, 0.f}); //{ 0.33f, 0.25f }, { 0.25f, 0.f }
		sy.material.diffuseTexture = std::make_unique<Texture>("textures/cubemap.jpg");
		auto& smz = *GeometryObject<Vertex>::makePlane(1, 1, 2, { 0.25f, 0.33f }, { 0.f, 0.33f });
		smz.material.diffuseTexture = std::make_unique<Texture>(skyboxTex);
		auto& sx = *GeometryObject<Vertex>::makePlane(1, 1, 2, { 0.25f, 0.33f }, { 0.25f, 0.33f });
		sx.material.diffuseTexture = std::make_unique<Texture>(skyboxTex);
		auto& sz = *GeometryObject<Vertex>::makePlane(1, 1, 2, { 0.25f, 0.33f }, { 0.5f, 0.33f });
		sz.material.diffuseTexture = std::make_unique<Texture>(skyboxTex);
		auto& smx = *GeometryObject<Vertex>::makePlane(1, 1, 2, { 0.25f, 0.33f }, { 0.75f, 0.33f });
		smx.material.diffuseTexture = std::make_unique<Texture>(skyboxTex);
		auto& smy = *GeometryObject<Vertex>::makePlane(1, 1, 2, { 0.25f, 0.33f }, { 0.25f, 0.66f });
		smy.material.diffuseTexture = std::make_unique<Texture>(skyboxTex);*/

		auto& sy = *GeometryObject<Vertex>::makePlane(1, 1, 2);
		sy.material.diffuseTexture = std::make_unique<Texture>("textures/cubemap_y.jpg");
		auto& smz = *GeometryObject<Vertex>::makePlane(1, 1, 2);
		smz.material.diffuseTexture = std::make_unique<Texture>("textures/cubemap_-z.jpg");
		auto& sx = *GeometryObject<Vertex>::makePlane(1, 1, 2);
		sx.material.diffuseTexture = std::make_unique<Texture>("textures/cubemap_x.jpg");
		auto& sz = *GeometryObject<Vertex>::makePlane(1, 1, 2);
		sz.material.diffuseTexture = std::make_unique<Texture>("textures/cubemap_z.jpg");
		auto& smx = *GeometryObject<Vertex>::makePlane(1, 1, 2);
		smx.material.diffuseTexture = std::make_unique<Texture>("textures/cubemap_-x.jpg");
		auto& smy = *GeometryObject<Vertex>::makePlane(1, 1, 2);
		smy.material.diffuseTexture = std::make_unique<Texture>("textures/cubemap_-y.jpg");

		// SHADERS -------------------------------------

		phongShader = ShaderProgram("shaders/phong.vert", "shaders/phong.frag");
		phongShader.setBinding(phongShader.uniformBlock("LightSourcesData"), 0);
		animShader = ShaderProgram("shaders/anim.vert", "shaders/phong.frag");
		animShader.setBinding(animShader.uniformBlock("LightSourcesData"), 0);
		glGenBuffers(1, &lightsUBO);

		// RENDER OBJECTS ----------------------------

		auto& ro_sy = renderObjects.emplace_back(sy, phongShader);
		ro_sy.lighted = false;
		ro_sy.nodes.push_back(&nd_sy);
		auto& ro_smz = renderObjects.emplace_back(smz, phongShader);
		ro_smz.lighted = false;
		ro_smz.nodes.push_back(&nd_smz);
		auto& ro_sx = renderObjects.emplace_back(sx, phongShader);
		ro_sx.lighted = false;
		ro_sx.nodes.push_back(&nd_sx);
		auto& ro_sz = renderObjects.emplace_back(sz, phongShader);
		ro_sz.lighted = false;
		ro_sz.nodes.push_back(&nd_sz);
		auto& ro_smx = renderObjects.emplace_back(smx, phongShader);
		ro_smx.lighted = false;
		ro_smx.nodes.push_back(&nd_smx);
		auto& ro_smy = renderObjects.emplace_back(smy, phongShader);
		ro_smy.lighted = false;
		ro_smy.nodes.push_back(&nd_smy);
	
		auto& roBuddha = renderObjects.emplace_back(geometryBuddha, phongShader);
		roBuddha.nodes.push_back(&buddhaNode);

		auto& trashCan = renderObjects.emplace_back(geomTrashCan, phongShader);
		auto& roTorus = renderObjects.emplace_back(geometryTorus, phongShader);

		auto& roPlane = renderObjects.emplace_back(geometryPlane, phongShader);
		roPlane.boundSphereRadius = 0.f;
		roPlane.transparent = true;
		roPlane.nodes.push_back(sceneRoot.get());
		planeId = roPlane.id;

		auto& roWizard = renderObjects.emplace_back(wizard, animShader);

		auto& roSun = renderObjects.emplace_back(geomSun, phongShader);
		roSun.lighted = false;
		roSun.uvOffset = glm::vec2(0.2f, 0.f);
		roSun.numTexFrames = 5;
		roSun.transparent = true;
		roSun.nodes.push_back(&sunNode);

		auto &roText = renderObjects.emplace_back(geomText, phongShader);
		roText.lighted = false;
		roText.transparent = true;
		roText.nodes.push_back(&textNode);

		characterAnimator = std::make_unique<CharacterAnimator>(roWizard, roPlane, &movedNode, worldMovedNode, renderObjects,
			glm::vec3(-150.f, 0.f, -150.f), glm::vec3(150.f, 50.f, 150.f));

		roTorus.nodes.insert(roTorus.nodes.end(), ufoModelNodes.begin(), ufoModelNodes.end());

		trashCan.nodes.insert(trashCan.nodes.end(), trashCanNodes.begin(), trashCanNodes.end());

		roWizard.nodes.push_back(&wizardNode);

		//---------------------------------------------

		OrbitCamera orbitCam;
		orbitCam.node = &camNode;
		orbitCam.far = 1000.f;
		orbitCam.orbitDistance = 15.0f;
		orbitCam.yaw = 180;
		orbitCam.pitch = -30;
		camera = std::make_unique<OrbitCamera>(orbitCam);

		EulerCamera eulerCam;
		eulerCam.node = &fpsCamNode;
		eulerCam.far = 1000.f;
		eulerCam.yaw = 180;
		fpsCamera = std::make_unique<EulerCamera>(eulerCam);

		auto& wizardLightNode = movedNode.addChild();
		wizardLightNode.transformLocal = glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 3.f, 0.f));
		auto& wizardLightNode1 = movedNode.addChild();
		wizardLightNode1.transformLocal = glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 0.f, 5.f));

		auto& dirLightNode = sceneRoot->addChild();
		dirLightNode.transformLocal = glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 0.1f, 1.f));
		
		LightColor col1;
		col1.diffuse = glm::vec4(.1f, .1f, .0f, .1f);
		col1.specular = glm::vec4(.1f);
		col1.ambient = glm::vec4(.01f);
		LightColor col2;
		//col2.diffuse = glm::vec4(.2f, .9f, .0f, 1.f);
		LightColor col3;
		col3.diffuse = glm::vec4(.0f, .0f, 1.f, 1.f);
		auto& dirLight = lightSources.emplace_back(new DirectionalLight(col1));
		dirLight->intensity = 0.6f;
		dirLight->nodes.push_back(&dirLightNode);
		auto& spotLight = lightSources.emplace_back(new SpotLight(col2, 45, 2));
		spotLight->intensity = 10;
		spotLight->nodes.push_back(&wizardLightNode);
		auto& pointLight = lightSources.emplace_back(new PointLight(col3));
		//pointLight->nodes.push_back(&wizardLightObj);

		fogTexture = Texture("textures/fog.png").createOGLObj();
	}
};

