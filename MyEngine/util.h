#pragma once
#include "global.h"

std::string readText(std::string fname);

std::vector<char> readBinary(const std::string& filename);

void writeBinary(const std::string& filename, const char* data, size_t size);