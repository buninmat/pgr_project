#pragma once
#include "global.h"

class SceneNode {

	std::list<SceneNode> children;

	void eval(glm::mat4& parentTransform) {
		if (cancelParentRotation) {
			glm::mat4 p = glm::mat4(1.f);
			p[3] = parentTransform[3];
			transform = p * transformLocal;
		}
		else {
			transform = parentTransform * transformLocal;
		}
		for (auto& child : children) {
			child.eval(transform);
		}
	}

public:
	glm::mat4 transformLocal = glm::mat4(1.0f);

	glm::mat4 transform = glm::mat4(1.0f);

	bool cancelParentRotation;

	int objectId = -1;

	bool disabled = false;

	SceneNode& addChild() {
		return children.emplace_back();
	}

	void evalRoot() {
		glm::mat4 root(1.0f);
		eval(root);
	}

	glm::vec3 getGlobalPos() {
		return transform[3];
	}

	glm::vec3 getLocalPos() {
		return transformLocal[3];
	}

	void setLocalPos(glm::vec3 val) {
		transformLocal[3] = glm::vec4(val, 1.f);
	}
};

/*
class SceneGraphObject {

	std::list<SceneGraphObject> children;

	std::unique_ptr<Camera> camera;

public:
	glm::vec3 translation{};

	glm::quat rotation{};

	std::unique_ptr<RenderObject> renderObject;

	Camera* addCamera(const Camera& camera) {

		*this->camera = camera;
		return this->camera.get();
	}

	SceneGraphObject& addChild() {
		return children.emplace_back();
	}

	void render(Camera& activeCamera) {

		if (renderObject.get() != nullptr) {
			renderObject->render(activeCamera);
		}
		if (camera.get() != nullptr) {

		}
	}
};
*/
