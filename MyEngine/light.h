#pragma once
#include "global.h"
#include "scene_graph.h"

enum LightSourceType { LIGHT_SOURCE_TYPE_DIRECTIONAL, LIGHT_SOURCE_TYPE_POINT, LIGHT_SOURCE_TYPE_SPOT };

struct LightColor {
	glm::vec4 ambient = glm::vec4(0.f, 0.f, 0.f, 1.f);
	glm::vec4 diffuse = glm::vec4(1.f);
	glm::vec4 specular = glm::vec4(1.f);
};

struct LightAttenuation {
	float constant = 0.f;
	float linear = 0.f;
	float quadratic = 1.f;
};

struct LightSourceData {
	glm::vec4 pos;
	glm::vec4 direction;

	LightColor color;
	LightAttenuation attenuation;

	float spot_cutoff;
	float spot_exp;

	float intensity;

	uint32_t type;

	float _;
};

struct LightSource {
	std::vector<SceneNode*> nodes;

	LightColor color;
	LightAttenuation attenuation;
	float intensity = 1;

	LightSource(LightColor color) : color(color) {};

	std::vector<LightSourceData> getData() {
		std::vector <LightSourceData> data;
		for (auto* node : nodes) {
			if (!node->disabled) {
				data.push_back(instantiate(node));
			}
		}
		return data;
	}

	virtual LightSourceData instantiate(SceneNode* node) = 0;
};

struct DirectionalLight : LightSource {
	DirectionalLight(LightColor color) : LightSource(color) {};

	virtual LightSourceData instantiate(SceneNode* node) {
		return { {}, node->transform[3], color, attenuation, 0, 0, intensity, LIGHT_SOURCE_TYPE_DIRECTIONAL };
	}
};

struct PointLight : LightSource {

	PointLight(LightColor color) : LightSource(color) {};

	virtual LightSourceData instantiate(SceneNode* node) {
		return { node->transform[3], {}, color, attenuation, 0, 0, intensity, LIGHT_SOURCE_TYPE_POINT };
	}
};

struct SpotLight : LightSource {
	float cutoff;
	float exp;

	SpotLight(LightColor color, float cutoff, float exp)
		:cutoff(cutoff), exp(exp), LightSource(color) {}

	virtual LightSourceData instantiate(SceneNode* node) {
		glm::vec3 direction = node->transform[1];
		return { node->transform[3], glm::vec4(direction, 0.f), color, attenuation, cutoff, exp, intensity,
				LIGHT_SOURCE_TYPE_SPOT };
	}
};

