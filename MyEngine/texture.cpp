#include "texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture::Texture(std::string file) {
	stbLoaded = true;
	data = stbi_load(file.data(), &width, &height, &nChannels, STBI_rgb_alpha);
}

Texture::Texture(Texture& other) {
	copied = true;
	data = other.data;
	width = other.width;
	height = other.height;
	nChannels = other.nChannels;
}

Texture::~Texture() {
	if (stbLoaded) {
		stbi_image_free(data);
	}
	else if(!copied) {
		delete[] data;
	}
}

Texture* Texture::makeSquare() {
	auto* tex = new Texture();
	tex->width = 50;
	tex->height = 50;
	tex->nChannels = 4;
	tex->data = new unsigned char[tex->width * tex->height * 4];
	const int edgeWidth = 2;
	for (int i = 0; i < tex->height; ++i) {
		for (int j = 0; j < tex->width; ++j) {
			bool edge = i > tex->height - edgeWidth || i < edgeWidth || j > tex->width - edgeWidth || j < edgeWidth;
			uint32_t pix = edge ? 0xff0606ff : 0x06060610;
			memcpy(tex->data + (i * tex->width + j) * 4, &pix, 4);
		}
	}
	return tex;
}

#include "fonts/font-win.h"

void renderChar(uint32_t* frame, int frameWidth, char c, int yrow, int xcolumn, int size,
	uint32_t forecolor, uint32_t backcolor) {
	font_descriptor_t font = FONT;
	int char_ind = c - font.firstchar;
	const uint16_t* bits = font.bits + char_ind * font.height;
	int width = font.width != 0 ? font.width[char_ind] : font.maxwidth;
	for (int i = 0; i < font.height; i++) {
		uint16_t row = bits[i];

		for (int j = 0; j < width; j++) {
			uint32_t color = ((row & 0x8000) != 0) ? forecolor : backcolor;
			for (int k = 0; k < size; ++k) {
				for (int m = 0; m < size; ++m) {
					int y = i * size + yrow + k;
					int x = j * size + xcolumn + m;
					frame[y * frameWidth + x] = color;
				}
			}

			row <<= 1;
		}
	}
}

int renderStr(uint32_t* frame, int frameWidth, const char* str, int yrow, int xcolumn, int size, int linespace,
	uint32_t forecolor, uint32_t backcolor) {

	font_descriptor_t font = FONT;
	int yoffset = 0;
	int xoffset = 0;
	for (int i = 0; i < strlen(str); i++) {
		if (str[i] == '\n') {
			yoffset += font.height * size + linespace;
			xoffset = 0;
			continue;
		}
		int char_ind = str[i] - font.firstchar;
		int is_valid = char_ind < font.size;
		if (is_valid) {
			renderChar(frame, frameWidth, str[i], yrow + yoffset, xcolumn + xoffset, size, forecolor, backcolor);
		}
		int width = font.width != 0 ? font.width[char_ind] : font.maxwidth;
		xoffset += width * size;
	}
	return yoffset;
}

Texture* Texture::makeText(std::stringstream& text, int width, int height) {

	uint32_t* frame = new uint32_t[width * height]{};

	Texture* texture = new Texture;
	texture->data = reinterpret_cast<unsigned char*>(frame);
	texture->width = width;
	texture->height = height;
	texture->nChannels = 4;

	renderStr(frame, width, text.str().data(), 0, 0, 1, 5, 0xfc'cc'00'cc, 0x0);
	return texture;
}

GLuint Texture::createOGLObj() {
	GLuint glTex;

	glGenTextures(1, &glTex);

	glBindTexture(GL_TEXTURE_2D, glTex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, data);

	glBindTexture(GL_TEXTURE_2D, 0);

	return glTex;
}