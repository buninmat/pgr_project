#include "render.h"

void Render::updateLightSources() {
	lightSourcesData.clear();
	for (auto& lightSource : lightSources) {
		auto data = lightSource->getData();
		lightSourcesData.insert(lightSourcesData.end(), data.begin(), data.end());
	}
}

void Render::bindLightsUBO() {
	glBindBuffer(GL_UNIFORM_BUFFER, lightsUBO);
	const size_t uboLightsSize = 100;
	struct {
		LightSourceData data[uboLightsSize];
		int size;
	} lightsData;
	lightsData.size = lightSourcesData.size();
	memcpy(lightsData.data, lightSourcesData.data(), std::min(uboLightsSize, lightSourcesData.size()) * sizeof(LightSourceData));
	glBufferData(GL_UNIFORM_BUFFER, sizeof(lightsData), &lightsData, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, lightsUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Render::processClick(int x, int y) {
	unsigned char objId = 0;
	glReadPixels(x, y, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &objId);
	if (objId != 0) {
		bool& disabled = clickableNodes->at(objId - 1)->disabled;
		disabled = !disabled;
	}
}

void Render::draw() {
	if (frameInd == 0) {
		for (auto& anim : nodeAnimators) {
			anim.start();
		}
	}
	static auto animStart = std::chrono::high_resolution_clock::now();
	auto end = std::chrono::high_resolution_clock::now();
	float frameTime = std::chrono::duration<float, std::chrono::milliseconds::period>(end - animStart).count();
	for (auto& anim : nodeAnimators) {
		anim.update();
	}
	characterAnimator->move(glm::vec3(0.f, -1.f, 0.f), 0.1f);
	sceneRoot->evalRoot();
	updateLightSources();
	if (frameInd != 0) {
		bindLightsUBO();
	}

	glm::vec3 camPos = glm::inverse(camera->getView())[3];

	std::vector<RenderObject*> rosSorted;
	for (auto& obj : renderObjects) {
		rosSorted.push_back(&obj);
	}
	int plId = planeId;
	std::sort(rosSorted.begin(), rosSorted.end(), [camPos, plId](RenderObject* a, RenderObject* b) {
		if (!b->transparent) {
			return false;
		}
		if (!a->transparent) {
			return true;
		}

		if (camPos.y > 0) {
			if (a->id == plId) {
				return true;
			}

			if (b->id == plId) {
				return false;
			}
		}
		else {
			if (a->id == plId) {
				return false;
			}

			if (b->id == plId) {
				return true;
			}
		}

		glm::vec3 apos = a->nodes[0]->getGlobalPos();
		glm::vec3 bpos = b->nodes[0]->getGlobalPos();
		float adist = glm::length(camPos - apos);
		float bdist = glm::length(camPos - bpos);
		return adist > bdist;
		});
	

	for (auto* obj : rosSorted) {
		obj->render(useFPSCam?*fpsCamera: *camera, lightsUBO, fogTexture, windowExtent, frameInd, frameTime / animFramePeriodSec);
	}

	if (frameTime > animFramePeriodSec) {
		animStart = std::chrono::high_resolution_clock::now();
		frameInd++;
	}
}